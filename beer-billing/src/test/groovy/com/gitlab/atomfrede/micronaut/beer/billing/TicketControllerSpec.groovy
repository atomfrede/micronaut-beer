package com.gitlab.atomfrede.micronaut.beer.billing

import com.gitlab.atomfrede.micronaut.beer.billing.client.HelloClient
import com.gitlab.atomfrede.micronaut.beer.billing.client.TicketControllerClient
import io.micronaut.context.ApplicationContext
import io.micronaut.context.Qualifier
import io.micronaut.http.HttpStatus
import io.micronaut.runtime.server.EmbeddedServer
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

class TicketControllerSpec extends Specification {

    @Shared
    @AutoCleanup
    EmbeddedServer embeddedServer = ApplicationContext.run(EmbeddedServer)

    @Shared
    TicketControllerClient client = embeddedServer
            .applicationContext
            .getBean(TicketControllerClient)

    def "should add new beer"() {

        given: "a beer item"
        def beer = new BeerItem("ipa", BeerItem.Size.MEDIUM)

        when: "request to add new beer to customer"
        def response = client.addBeerToCustomerBill(beer, "fred")

        then: "beer should be added to bill"
        response.code() == HttpStatus.OK.code
    }
}
