package com.gitlab.atomfrede.micronaut.beer.billing.client;

import com.gitlab.atomfrede.micronaut.beer.billing.BeerItem;
import com.gitlab.atomfrede.micronaut.beer.billing.Ticket;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.client.Client;
import io.micronaut.retry.annotation.Retryable;
import io.reactivex.Single;

import javax.validation.constraints.NotBlank;

@Client("/billing")
public interface TicketControllerClient {

    @Get("/reset/{customerName}")
    HttpResponse resetCustomerBill(@NotBlank String customerName);

    @Post("/addBeer/{customerName}")
    Single<BeerItem> addBeerToCustomerBill(@Body BeerItem beer, @NotBlank String customerName);


    @Get("/bill/{customerName}")
    Single<Ticket> bill(@NotBlank String customerName);
}
