package com.gitlab.atomfrede.micronaut.beer.billing;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.tracing.annotation.ContinueSpan;
import io.micronaut.tracing.annotation.SpanTag;
import io.micronaut.validation.Validated;
import io.reactivex.Single;

import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Optional;

@Controller("/billing")
@Validated
public class TicketController {

    HashMap<String, Ticket> billsPerCustomer = new HashMap<>();

    @Get("/reset/{customerName}")
    public HttpResponse resetCustomerBill(@NotBlank String customerName) {
        billsPerCustomer.put(customerName, new Ticket());
        return HttpResponse.ok();
    }

    @ContinueSpan
    @Post("/addBeer/{customerName}")
    public Single<BeerItem> addBeerToCustomerBill(@Body BeerItem beer, @SpanTag("customer.name") @NotBlank String customerName) {
        Optional<Ticket> optionalTicket = Optional.ofNullable(billsPerCustomer.get(customerName));

        Ticket ticket = optionalTicket.orElseGet(Ticket::new);
        ticket.add(beer);

        billsPerCustomer.put(customerName, ticket);
        return Single.just(beer);
    }

    @ContinueSpan
    @Get("/bill/{customerName}")
    public Single<Ticket> bill(@NotBlank @SpanTag("customer.name") String customerName) {
        Optional<Ticket> t = Optional.ofNullable(billsPerCustomer.get(customerName));
        Ticket ticket = t.orElseGet(Ticket::new);
        return Single.just(ticket);
    }
}
