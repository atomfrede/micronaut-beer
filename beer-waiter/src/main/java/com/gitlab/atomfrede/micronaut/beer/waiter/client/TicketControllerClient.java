package com.gitlab.atomfrede.micronaut.beer.waiter.client;

import com.gitlab.atomfrede.micronaut.beer.waiter.Beer;
import com.gitlab.atomfrede.micronaut.beer.waiter.Ticket;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.client.Client;
import io.micronaut.retry.annotation.Retryable;
import io.reactivex.Single;

import javax.validation.constraints.NotBlank;

@Client(id = "billing", path = "/billing")
@Retryable(attempts = "5", delay = "2s")
public interface TicketControllerClient {

    @Get("/reset/{customerName}")
    HttpResponse resetCustomerBill(@NotBlank String customerName);

    @Post("/addBeer/{customerName}")
    Single<Beer> addBeerToCustomerBill(@Body Beer beer, @NotBlank String customerName);


    @Get("/bill/{customerName}")
    Single<Ticket> bill(@NotBlank String customerName);

}
