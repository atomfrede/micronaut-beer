package com.gitlab.atomfrede.micronaut.beer.waiter.client;

import com.gitlab.atomfrede.micronaut.beer.waiter.Beer;
import com.gitlab.atomfrede.micronaut.beer.waiter.CustomerBill;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.Client;
import io.micronaut.retry.annotation.CircuitBreaker;
import io.reactivex.Single;

import javax.validation.constraints.NotBlank;

@Client("/waiter")
@CircuitBreaker(delay = "1s", attempts = "5", multiplier = "3", reset = "100s")
public interface WaiterControllerClient {

    @Get("/beer/{customerName}")
    Single<Beer> serveBeerToCustomer(@NotBlank String customerName);

    @Get("/bill/{customerName}")
    Single<CustomerBill> bill(@NotBlank String customerName);
}