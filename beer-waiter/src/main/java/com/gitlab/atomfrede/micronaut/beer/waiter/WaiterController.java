package com.gitlab.atomfrede.micronaut.beer.waiter;

import com.gitlab.atomfrede.micronaut.beer.waiter.Beer.Size;
import com.gitlab.atomfrede.micronaut.beer.waiter.client.TicketControllerClient;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.tracing.annotation.NewSpan;
import io.micronaut.tracing.annotation.SpanTag;
import io.micronaut.validation.Validated;
import io.reactivex.Single;

import javax.validation.constraints.NotBlank;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@Controller("/waiter")
@Validated
public class WaiterController {

    TicketControllerClient ticketControllerClient;

    public WaiterController(TicketControllerClient ticketControllerClient) {
        this.ticketControllerClient = ticketControllerClient;
    }

    @NewSpan("serve-beer")
    @Get("/beer/{customerName}")
    public Single<Beer> serveBeerToCustomer(@NotBlank String customerName) {
        return ticketControllerClient.addBeerToCustomerBill(new Beer("ipa", Beer.Size.MEDIUM), customerName);
    }

    @NewSpan("bill-customer")
    @Get("/bill/{customerName}")
    public Single<CustomerBill> bill(@SpanTag("customer.name") @NotBlank String customerName) {

        Single<Ticket> singleTicket = ticketControllerClient.bill(customerName);

        return singleTicket.map(ticket -> {
            CustomerBill bill = new CustomerBill(ticket.getCost());
            bill.setDeskId(ticket.getDeskId());
            return bill;
          }
        );
    }
}
