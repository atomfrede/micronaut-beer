package com.gitlab.atomfrede.micronaut.beer.waiter;

import java.util.ArrayList;
import java.util.List;

public class Ticket {

    private int deskId;
    private double cost = 0;
    private List<Beer> items = new ArrayList<>();

    public Ticket() {
        cost = 0;
    }


    public int getDeskId() {
        return deskId;
    }

    public void setDeskId(int deskId) {
        this.deskId = deskId;
    }

    public double getCost() {
        items.forEach(beer -> cost += beer.getCost());
        return cost;
    }

    public void add(Beer beer) {
        items.add(beer);
    }
}
