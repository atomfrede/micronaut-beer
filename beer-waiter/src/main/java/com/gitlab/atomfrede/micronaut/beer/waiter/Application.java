package com.gitlab.atomfrede.micronaut.beer.waiter;

import io.micronaut.runtime.Micronaut;

public class Application {

    public static void main(String[] args) {
        Micronaut.run(Application.class);
    }
}